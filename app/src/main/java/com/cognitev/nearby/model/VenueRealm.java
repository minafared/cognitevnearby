package com.cognitev.nearby.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by mina fared on 9/21/2017.
 */

/*
* using this simple class of venue to save data offline in realm database
 */
public class VenueRealm extends RealmObject {

    @PrimaryKey
    private String id;
    private String name;
    private String image;
    private String address;


    /*
    name setter & getter
     */
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /*
    id setter & getter
     */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    /*
    image setter & getter
     */
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }



    /*
    address setter & getter
     */

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


}
