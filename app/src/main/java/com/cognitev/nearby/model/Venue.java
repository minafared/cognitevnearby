package com.cognitev.nearby.model;

import com.cognitev.nearby.app.Constants;

import org.json.JSONException;

import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by mina fared on 9/21/2017.
 */

public class Venue {


    private String id;
    private String name;
    private VenuesResponse.Contact contact;
    private VenuesResponse.Location location;
    private List<VenuesResponse.Category> categories;
    private boolean verified;
    private VenuesResponse.Stats stats;
    private VenuesResponse.Price price;
    private double rating;
    private String ratingColor;
    private int ratingSignals;
    private VenuesResponse.Menu menu;
    private boolean allowMenuUrlEdit;
    private VenuesResponse.BeenHere beenHere;
    private VenuesResponse.Photos photos;
    private VenuesResponse.HereNow hereNow;
    private VenuesResponse.Hours hours;
    public String address;
    public String image;


    public String getImageURL() {
        if (image != null)
            return image;

        String Image = "";
        try {
            Image = categories.get(0).icon.prefix + Constants.IMAGE_SIZE + categories.get(0).icon.suffix;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Image;

    }

    public String getName() {
        return this.name;
    }

    public String getId() {
        return this.id;
    }

    public String getAddress() {
        if (location != null)
            return location.address;
        else
            return address;
    }


    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setImage(String image) {
        this.image = image;
    }


}
