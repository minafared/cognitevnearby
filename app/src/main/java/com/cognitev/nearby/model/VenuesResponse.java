package com.cognitev.nearby.model;

import java.util.List;

/**
 * Created by mina fared on 9/22/2017.
 */

@SuppressWarnings("all")
public class VenuesResponse {


    public Meta meta;
    public Response response;

    public class Response {
        public SuggestedFilters suggestedFilters;
        public Warning warning;
        public String headerLocation;
        public String headerFullLocation;
        public String headerLocationGranularity;
        public int totalResults;
        public SuggestedBounds suggestedBounds;
        public List<Group> groups;
    }

    public Response getVenues() {
        return response;
    }

    public class Meta {
        public int code;
        public String requestId;
    }

    public class Filter {
        public String name;
        public String key;
    }

    public class SuggestedFilters {
        public String header;
        public List<Filter> filters;
    }

    public class Warning {
        public String text;
    }

    public class Ne {
        public double lat;
        public double lng;
    }

    public class Sw {
        public double lat;
        public double lng;
    }

    public class SuggestedBounds {
        public Ne ne;
        public Sw sw;
    }

    public class Item2 {
        public String summary;
        public String type;
        public String reasonName;
    }

    public class Reasons {
        public int count;
        public List<Item2> items;
    }

    public class Contact {
        public String phone;
        public String formattedPhone;
        public String instagram;
        public String facebook;
        public String facebookUsername;
        public String facebookName;
    }

    public class LabeledLatLng {
        public String label;
        public double lat;
        public double lng;
    }

    public class Location {
        public String address;
        public String crossStreet;
        public double lat;
        public double lng;
        public List<LabeledLatLng> labeledLatLngs;
        public int distance;
        public String cc;
        public String city;
        public String state;
        public String country;
        public List<String> formattedAddress;
    }

    public class Icon {
        public String prefix;
        public String suffix;
    }

    public class Category {
        public String id;
        public String name;
        public String pluralName;
        public String shortName;
        public Icon icon;
        public boolean primary;
    }

    public class Stats {
        public int checkinsCount;
        public int usersCount;
        public int tipCount;
    }

    public class Price {
        public int tier;
        public String message;
        public String currency;
    }

    public class Menu {
        public String type;
        public String label;
        public String anchor;
        public String url;
        public String mobileUrl;
        public String externalUrl;
    }

    public class BeenHere {
        public int count;
        public boolean marked;
        public int lastCheckinExpiredAt;
    }

    public class Photos {
        public int count;
        public List<Photo> groups;
    }

    public class HereNow {
        public int count;
        public String summary;
        public List<Group> groups;
    }

    public class RichStatus {
        //public List<object> entities ;
        public String text;
    }

    public class Hours {
        public String status;
        public RichStatus richStatus;
        public boolean isOpen;
        public boolean isLocalHoliday;
    }

    public class Todo {
        public int count;
    }

    public class Photo {
        public String prefix;
        public String suffix;
    }

    public class User {
        public String id;
        public String firstName;
        public String lastName;
        public String gender;
        public Photo photo;
    }

    public class Likes {
        public int count;
        public List<Group> groups;
        public String summary;
    }

    public class Tip {
        public String id;
        public int createdAt;
        public String text;
        public String type;
        public String canonicalUrl;
        public boolean logView;
        public int agreeCount;
        public int disagreeCount;
        public Todo todo;
        public User user;
        public Likes likes;
    }

    public class Item {
        public Reasons reasons;
        public Venue venue;
        public List<Tip> tips;
        public String referralId;
    }

    public class Group {
        public String type;
        public String name;
        public List<Item> items;
    }


}
