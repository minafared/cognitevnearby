package com.cognitev.nearby.app;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;

import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by mina fared on 9/21/2017.
 */
public class App extends Application {


    @Override
    public void onCreate() {
        super.onCreate();

        /*
        * initialise Realm database
         */
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);


    }


    @Override
    public void onTerminate() {
        super.onTerminate();
    }


}
