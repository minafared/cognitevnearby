package com.cognitev.nearby.app;

/**
 * Created by mina fared on 9/21/2017.
 */

public class Constants {

    public static final String BASE_URL = "https://api.foursquare.com/v2/";

    public static final String CLIENT_ID = "YFRT0F0EKMLCUYYQO04YWQMUN15MXZH2KEXU5ZGN2D1QV0CF";

    public static final String CLIENT_SECRET = "I2NQKHQPPL3DCCBTWU4PP4ZF2WJMGYZEOXIVXSHF0K330RJA";

    public static final String VERSION = "20170921"; //YYYYMMDD

    public static final String RADIUS = "1000"; //1000 Meter radius search.

    public static final String IMAGE_SIZE = "64"; // venue image size

    public static final int SUCCESS = 200;

    public static final int REALTIME_MODE = 1;

    public static final int LOCATION_PERMISSION_CODE = 1000;

    public static final int SINGLE_UPDATE_MODE = 2;

    public static final String PREFS = "prefs";

    public static final int ITEM_PAGE = 20;



}
