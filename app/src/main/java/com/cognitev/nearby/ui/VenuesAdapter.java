package com.cognitev.nearby.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.cognitev.nearby.R;
import com.cognitev.nearby.model.Venue;
import com.cognitev.nearby.model.VenuesResponse;
import com.cognitev.nearby.ui.viewHolders.LoadingViewHolder;
import com.cognitev.nearby.ui.viewHolders.VenuesViewHolder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mina Fared on 24/09/2017.
 */
@SuppressWarnings("all")
public class VenuesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    final int VIEW_TYPE_LOADING = 1;
    final int VIEW_TYPE_VENUE = 2;

    public Context context;

    public List<Venue> venues = new ArrayList<>();

    int position;

    public VenuesAdapter(Context context, List<Venue> venues) {
        this.context = context;
        this.venues = venues;
    }

    public int getItemCount() {
        return venues.size();
    }

    @Override
    public int getItemViewType(int position) {
        return venues.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_VENUE;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View v = null;
        switch (viewType) {
            case VIEW_TYPE_VENUE:
                v = inflater.inflate(R.layout.item_venue, parent, false);
                viewHolder = new VenuesViewHolder(v);
                break;
            case VIEW_TYPE_LOADING:
                v = inflater.inflate(R.layout.item_loading, parent, false);
                viewHolder = new LoadingViewHolder(v);
                break;
            default:
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int itemType = getItemViewType(position);
        this.position = position;
        switch (itemType) {
            case VIEW_TYPE_VENUE:
                ((VenuesViewHolder) holder).onBindView(this, position);
                break;
            case VIEW_TYPE_LOADING:
                ((LoadingViewHolder) holder).onBindView(this, position);
                break;
            default:
                break;
        }
    }


}