package com.cognitev.nearby.ui;

import android.app.Activity;
import android.content.Context;

import com.cognitev.nearby.app.Constants;
import com.cognitev.nearby.model.Venue;
import com.cognitev.nearby.model.VenueRealm;
import com.cognitev.nearby.model.VenuesResponse;
import com.cognitev.nearby.network.Api;
import com.cognitev.nearby.utils.RealmController;
import com.cognitev.nearby.utils.Utils;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mina fared on 9/24/2017.
 */

public class VenuesPresenterImpl implements VenuesPresenter {

    private VenuesView view;
    private boolean isUpdating;
    private Context context;
    private Realm realm;

    @Override
    public void setView(Context context, VenuesView view) {
        this.view = view;
        this.context = context;
    }

    /*
    * get venues online from foursquare api
     */
    @Override
    public void getVenues(int page, final boolean reload, final boolean isSwipeRefreshLayout, String latitude, String longitude) {

        // to prevent more than request at the same time
        if (isUpdating) {
            return;
        }
        isUpdating = true;

        if (!isSwipeRefreshLayout)
            view.showLoading(reload);


        if (!Utils.isOnline(context)) {
            view.hideLoading();
            return;
        }


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        Api api = retrofit.create(Api.class);

        Observable<VenuesResponse> friendResponseObservable = api.getVenues(Constants.CLIENT_ID, Constants.CLIENT_SECRET, Constants.VERSION, latitude + "," + longitude, page, Constants.ITEM_PAGE, Constants.RADIUS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        friendResponseObservable.subscribe(new Observer<VenuesResponse>() {


            @Override
            public void onError(Throwable e) {
                view.showErrorMessage();
                view.hideLoading();

                // enable update again
                isUpdating = false;
            }

            @Override
            public void onComplete() {
                view.hideLoading();
                // enable update again
                isUpdating = false;
            }

            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(VenuesResponse response) {

                if (response.getVenues().groups.size() > 0) {

                    List<VenuesResponse.Item> items = response.response.groups.get(0).items;
                    List<Venue> venues = new ArrayList<>();
                    for (int i = 0; i < items.size(); i++) {
                        venues.add(items.get(i).venue);
                    }

                    int totalResults = response.response.totalResults;
                    if (venues.size() > 0) {
                        view.showVenues(venues, reload, totalResults);

                        if (page == 0)// insert venues in database in first page only
                            insertVenuesinDataBase(venues);
                    } else
                        view.showEmptyResult();

                } else
                    view.showEmptyResult();
            }
        });


    }

  /*
  * get venues offline from Realm database
  */

    public void getOfflineVenues() {

        //get realm instance
        this.realm = RealmController.with((Activity) context).getRealm();

        // refresh realm
        RealmController.with((Activity) context).refresh();

        // check if there is data in real db
        if (!RealmController.with((Activity) context).hasVenueRealms())
            return;
        // get venues
        RealmResults<VenueRealm> realmResults = RealmController.with((Activity) context).getVenueRealms();
        List<Venue> venues = new ArrayList<Venue>();


        if (realmResults.size() > 0) {

            for (int i = 0; i < realmResults.size(); i++) {

                Venue venue = new Venue();
                venue.setName(realmResults.get(i).getId());
                venue.setImage(realmResults.get(i).getImage());
                venue.setAddress(realmResults.get(i).getAddress());
                venue.setId(realmResults.get(i).getId());
                venues.add(venue);

            }
            view.showVenues(venues, true, realmResults.size());
        }

    }


   /*
   * insert venues in Realm database
   */

    private void insertVenuesinDataBase(List<Venue> items) {

        //get realm instance
        this.realm = RealmController.with((Activity) context).getRealm();

        // refresh the realm instance
        RealmController.with((Activity) context).refresh();

        // clear old data
        RealmController.with((Activity) context).clearAll();

        for (Venue venue : items) {
            VenueRealm venueRealm = new VenueRealm();
            venueRealm.setId(venue.getName());
            venueRealm.setName(venue.getName());
            venueRealm.setAddress(venue.getAddress());
            venueRealm.setImage(venue.getImageURL());

            try {
                realm.beginTransaction();
                realm.copyToRealm(venueRealm);
                realm.commitTransaction();
            } catch (Exception e) {

            }

        }
    }

}
