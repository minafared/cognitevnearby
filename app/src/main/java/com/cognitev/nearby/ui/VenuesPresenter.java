package com.cognitev.nearby.ui;

import android.content.Context;

/**
 * Created by mina fared on 9/22/2017.
 */

public interface VenuesPresenter {

    void setView(Context context,VenuesView view);

    void getVenues( int page, boolean reload, boolean isSwipeRefreshLayout, String latitude, String longitude);
    void getOfflineVenues();
}
