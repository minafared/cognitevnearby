package com.cognitev.nearby.ui.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognitev.nearby.R;
import com.cognitev.nearby.model.Venue;
import com.cognitev.nearby.ui.VenuesAdapter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mina on 27/09/17.
 */

public class VenuesViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.venue_tv)
    TextView venue_tv;

    @BindView(R.id.venue_iv)
    ImageView venue_iv;

    @BindView(R.id.venueLocation_tv)
    TextView venueLocation_tv;

    public VenuesViewHolder(android.view.View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }


    public void onBindView(VenuesAdapter adapter, int position) {
        Venue venue = adapter.venues.get(position);
        venueLocation_tv.setText(venue.getAddress());
        venue_tv.setText(venue.getName());

        Picasso.with(adapter.context)
                .load(venue.getImageURL())
                .error(R.drawable.location_ic)
                .into(venue_iv);

    }
}