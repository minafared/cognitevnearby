package com.cognitev.nearby.ui.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.cognitev.nearby.R;
import com.cognitev.nearby.ui.VenuesAdapter;

/**
 * Created by mina on 27/09/17.
 */

public class LoadingViewHolder extends RecyclerView.ViewHolder {

    public LoadingViewHolder(View view) {
        super(view);
     }

    public void onBindView(VenuesAdapter adapter, int position) {

    }
}