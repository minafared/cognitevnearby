
package com.cognitev.nearby.ui;


import com.cognitev.nearby.model.Venue;
import com.cognitev.nearby.model.VenuesResponse;

import java.util.List;

/**
 * Created by mina fared on 9/22/2017.
 */

public interface VenuesView {

    void showLoading(boolean reload);

    void hideLoading();

    void showVenues(List<Venue> venues, boolean reload, int total);

    void showErrorMessage();

    void showEmptyResult();

}
