package com.cognitev.nearby.ui;

import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.cognitev.nearby.R;
import com.cognitev.nearby.app.Constants;
import com.cognitev.nearby.model.Venue;
import com.cognitev.nearby.utils.EndlessRecyclerViewScrollListener;
import com.cognitev.nearby.utils.Utils;
import com.cognitev.nearby.utils.location.LocationService;
import com.cognitev.nearby.utils.UserPreferences;
import com.cognitev.nearby.utils.location.onLocationListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.hoang8f.android.segmented.SegmentedGroup;
import jp.wasabeef.recyclerview.adapters.AnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

public class VenuesActivity extends BaseActivity implements VenuesView, RadioGroup.OnCheckedChangeListener,
        onLocationListener, SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    VenuesPresenter presenter;

    @BindView(R.id.segmentedGroup)
    SegmentedGroup segmentedGroup;

    @BindView(R.id.realTime_rb)
    RadioButton realTime_rb;

    @BindView(R.id.singleUpdate_rb)
    RadioButton singleUpdate_rb;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.noDataLayout)
    LinearLayout noDataLayout;

    @BindView(R.id.errorLayout)
    LinearLayout errorLayout;

    boolean isLoading;
    int totalResults;

    List<Venue> items;
    VenuesAdapter adapter;
    UserPreferences userPreferences;
    LocationService locationService;
    int startingPage = 0;
    private static final int GRID_SPAN = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        initialiseRecycleView();


        //user preferences
        userPreferences = new UserPreferences(this);

        //presenter
        presenter = new VenuesPresenterImpl();
        presenter.setView(this, this);


        //segmentedGroup
        segmentedGroup.setOnCheckedChangeListener(this);
        realTime_rb.setChecked(userPreferences.getUserMode() == Constants.REALTIME_MODE);
        singleUpdate_rb.setChecked(userPreferences.getUserMode() == Constants.SINGLE_UPDATE_MODE);


        //swipeRefreshLayout
        swipeRefreshLayout.setOnRefreshListener(this);


        //onClickListeners
        noDataLayout.setOnClickListener(this);
        errorLayout.setOnClickListener(this);

        loadOfflineData();

    }

    private void getCurrentLocationUpdateVenues(boolean isSwipeRefreshLayout) {
        if (locationService == null)
            locationService = new LocationService(this, this, isSwipeRefreshLayout);
        else
            locationService.getLocation();

        if (!isSwipeRefreshLayout)
            showLoading(true);
    }

    private void initialiseRecycleView() {
        items = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(false);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(final int page, int totalItemsCount) {

                isLoading = true;

                if (totalItemsCount < totalResults) // check to disable pagination .its now accurate way due to some duplicated results.
                {
                    presenter.getVenues(page, false, false, userPreferences.getLatitude(), userPreferences.getLongitude());
                    items.add(null);
                    adapter.notifyItemInserted(items.size() - 1);
                }


            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }


        });


    }


    /*
 * SegmentedGroup OnCheckedChangeListener
 */
    @Override
    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {
        switch (checkedId) {
            case R.id.realTime_rb:
                userPreferences.setUserMode(Constants.REALTIME_MODE);

                // to enable gps listener again if it is disabled by single update mode
                if (locationService != null)
                    locationService.getLocation();

                break;
            case R.id.singleUpdate_rb:
                userPreferences.setUserMode(Constants.SINGLE_UPDATE_MODE);
                break;
            default:
                break;
        }

        //CheckingNetworkAndGPS("onCheckedChanged");
    }


    /*
 * VenuesView
 */
    @Override
    public void showLoading(boolean reload) {
        if (!swipeRefreshLayout.isRefreshing() && reload)
            progressBar.setVisibility(View.VISIBLE);

        recyclerView.setVisibility(View.VISIBLE);
        errorLayout.setVisibility(View.GONE);
        noDataLayout.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void showVenues(List<Venue> venues, boolean reload, int totalResults) {

        if (items.size() > 0) {
            items.remove(items.size() - 1);
            adapter.notifyItemRemoved(items.size());
        }


        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);


        this.totalResults = totalResults;
        isLoading = false;// enable loading more.

        if (reload) {
            items = new ArrayList<>();
            items.addAll(venues);

            adapter = new VenuesAdapter(this, items);

            //add custom animation to recycleView.
            AnimationAdapter animationAdapter = new ScaleInAnimationAdapter(adapter);
            animationAdapter.setFirstOnly(false);
            animationAdapter.setDuration(800);
            animationAdapter.setInterpolator(new OvershootInterpolator(.5f));
            recyclerView.setAdapter(animationAdapter);
        } else {
            items.addAll(venues);
            adapter.notifyDataSetChanged();

        }


    }


    @Override
    public void showErrorMessage() {
        progressBar.setVisibility(View.GONE);

        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);

        if (items.size() == 0) {
            errorLayout.setVisibility(View.VISIBLE);
            noDataLayout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);

        }


    }

    @Override
    public void showEmptyResult() {
        progressBar.setVisibility(View.GONE);

        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);

        // check if there is no data to show and hide views
        if (items.size() == 0) {
            errorLayout.setVisibility(View.GONE);
            noDataLayout.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);

        }
    }


    /*
    *location listener.
     */


    @Override
    public void onLocationChanged(Location location, boolean isSwipeRefreshLayout) {
        presenter.getVenues(startingPage, true, isSwipeRefreshLayout, String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));

        //in single update mode, disable realTime update when location is updated.
        if (userPreferences.getUserMode() == Constants.SINGLE_UPDATE_MODE) {
            locationService.stopUpdateUserLocations();// remove updates while while single update mode to save battery :D ;)
        }
    }


    /*
    *SwipeRefreshLayout OnRefreshListener.
     */

    @Override
    public void onRefresh() {
        CheckingNetworkAndGPS();
        errorLayout.setVisibility(View.GONE);
        noDataLayout.setVisibility(View.GONE);

        if (!Utils.isOnline(this))
            swipeRefreshLayout.setRefreshing(false);
    }


   /*
    * onRequest Permissions Result.
    */

    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            if (requestCode == Constants.LOCATION_PERMISSION_CODE) {
                getCurrentLocationUpdateVenues(false);
            }

    }


    /*
     * super functions from baseActivity
     */


    @Override
    public void WhenAllIsGood() {
        super.WhenAllIsGood();

        //location service
        getCurrentLocationUpdateVenues(swipeRefreshLayout.isRefreshing());
    }

    @Override
    public void onClick(View view) {
        hideLoading();
        switch (view.getId()) {
            case R.id.errorLayout:
                presenter.getVenues(startingPage, true, false, String.valueOf(userPreferences.getLatitude()), String.valueOf(userPreferences.getLongitude()));
                break;
            case R.id.noDataLayout:
                presenter.getVenues(startingPage, true, false, String.valueOf(userPreferences.getLatitude()), String.valueOf(userPreferences.getLongitude()));
                break;
            default:
                break;
        }
    }


    @Override
    public void loadOfflineData() {
        super.loadOfflineData();
        // check if there is no data loaded in recycleView to load offline data
        if (items.size() == 0)
            presenter.getOfflineVenues();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (locationService != null) {
            locationService.stopUpdateUserLocations();// remove updates while while single update mode to save battery :D ;)
            locationService = null;
        }


    }
}
