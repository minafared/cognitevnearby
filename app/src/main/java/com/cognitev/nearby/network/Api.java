package com.cognitev.nearby.network;

import com.cognitev.nearby.model.VenuesResponse;


import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by mina fared on 9/21/2017.
 */
public interface Api {


    @GET("venues/explore")
    Observable<VenuesResponse> getVenues(@Query("client_id") String client_id,
                                         @Query("client_secret") String client_secret,
                                         @Query("v") String version,
                                         @Query("ll") String latitudeLongitude,
                                         @Query("offset") int offset,
                                         @Query("limit") int limit,
                                         @Query("radius") String radius);


}
