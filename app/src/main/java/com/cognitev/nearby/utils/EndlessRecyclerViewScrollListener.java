package com.cognitev.nearby.utils;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by mina fared on 9/23/2017.
 */

public abstract class EndlessRecyclerViewScrollListener extends RecyclerView.OnScrollListener {

    private int currentPage = 0;
    private LinearLayoutManager layoutManager;

    protected EndlessRecyclerViewScrollListener(LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    @Override
    public void onScrolled(RecyclerView view, int dx, int dy) {
        if (dy > 0) //check for scroll down
        {
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();


            if (!isLoading())
                if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                    currentPage++;
                    onLoadMore(currentPage, totalItemCount);

                }
        }
    }

    public abstract void onLoadMore(int page, int totalItemsCount);

    public abstract boolean isLoading();
}
