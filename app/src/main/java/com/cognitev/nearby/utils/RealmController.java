package com.cognitev.nearby.utils;

/**
 * Created by mina fared on 9/28/2017.
 */

import android.app.Activity;
import android.app.Application;

import com.cognitev.nearby.model.VenueRealm;

import io.realm.Realm;
import io.realm.RealmResults;

@SuppressWarnings("all")
public class RealmController {

    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }


    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }


    public Realm getRealm() {

        return realm;
    }

    //Refresh the realm istance
    public void refresh() {

        realm.refresh();
    }

    //clear all objects from VenueRealm.class
    public void clearAll() {

        realm.beginTransaction();
        realm.clear(VenueRealm.class);
        realm.commitTransaction();
    }

    //find all objects in the VenueRealm.class
    public RealmResults<VenueRealm> getVenueRealms() {

        return realm.where(VenueRealm.class).findAll();
    }

    //query a single item with the given id
    public VenueRealm getVenueRealm(String id) {

        return realm.where(VenueRealm.class).equalTo("id", id).findFirst();
    }

    //check if VenueRealm.class is empty
    public boolean hasVenueRealms() {

        return !realm.allObjects(VenueRealm.class).isEmpty();
    }



}