package com.cognitev.nearby.utils.location;

import android.location.Location;

/**
 * Created by mina fared on 9/24/2017.
 */

public interface onLocationListener {

      void onLocationChanged(Location location,boolean isSwipeRefreshLayout);
}
