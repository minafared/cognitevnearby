package com.cognitev.nearby.utils.location;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;

/**
 * Created by mina fared on 9/25/2017.
 */
public class LocationStatusReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            ((LocationStatusListener) context).onLocationSuccess();

        } else
            ((LocationStatusListener) context).onLocationFail();

    }

    public interface LocationStatusListener {
        void onLocationFail();

        void onLocationSuccess();
    }
}


