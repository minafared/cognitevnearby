package com.cognitev.nearby.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.cognitev.nearby.app.Constants;

/**
 * Created by mina fared on 9/24/2017.
 */

public class UserPreferences {
    private SharedPreferences mPref;
    private SharedPreferences.Editor editor;

    public UserPreferences(Context mContext) {
        mPref = mContext.getSharedPreferences(Constants.PREFS, Context.MODE_PRIVATE);
        editor = mPref.edit();
    }

    /*
    * set last user location
     */
    public void setLocation(String latitude, String longitude) {
        SharedPreferences.Editor editor = mPref.edit();
        editor.putString("latitude", latitude);
        editor.putString("longitude", longitude);
        editor.apply();
    }

    /*
    * set user mode
     */
    public void setUserMode(int mode) {
        SharedPreferences.Editor editor = mPref.edit();
        editor.putInt("mode", mode);
        editor.apply();
    }

    /*
    *get user mode
     */
    public int getUserMode() {
        return mPref.getInt("mode", Constants.REALTIME_MODE);
    }

    /*
    * get user latitude
     */
    public String getLatitude() {
        return mPref.getString("latitude", "");
    }

    /*
    *get user longitude
     */

    public String getLongitude() {
        return mPref.getString("longitude", "");
    }


}
